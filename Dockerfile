# This file is a template, and might need editing before it works on your project.
FROM openjdk:8-alpine

VOLUME /tmp
ADD /target/ci-java-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
